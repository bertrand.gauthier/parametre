<?php

namespace UnicaenParametre\Entity\Db;

class Categorie
{
    const DEFAULT_ORDER = 9999;

    private ?int $id = null;
    private ?string $code = null;
    private ?string $libelle = null;
    private ?string $description = null;
    private ?int $ordre = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): Categorie
    {
        $this->code = $code;
        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): Categorie
    {
        $this->libelle = $libelle;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): Categorie
    {
        $this->description = $description;
        return $this;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(int $ordre = Categorie::DEFAULT_ORDER): Categorie
    {
        $this->ordre = $ordre;
        return $this;
    }

}

