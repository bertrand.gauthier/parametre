<?php

namespace UnicaenParametre\Entity\Db;

class Parametre
{
    const DEFAULT_ORDER = 9999;

    const TYPE_STRING   = "String";
    const TYPE_BOOLEAN  = "Boolean";
    const TYPE_NUMBER   = "Number";

    private ?int $id = null;
    private ?Categorie $categorie = null;
    private ?string $code = null;
    private ?string $libelle = null;
    private ?string $description = null;
    private ?string $valeur = null;
    private ?string $valeurs_possibles = null;
    private ?int $ordre = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): Parametre
    {
        $this->categorie = $categorie;
        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): Parametre
    {
        $this->code = $code;
        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): Parametre
    {
        $this->libelle = $libelle;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): Parametre
    {
        $this->description = $description;
        return $this;
    }

    public function getValeur(): ?string
    {
        return $this->valeur;
    }

    public function setValeur(?string $valeur): Parametre
    {
        $this->valeur = $valeur;
        return $this;
    }

    public function getValeursPossibles(): ?string
    {
        return $this->valeurs_possibles;
    }

    public function setValeursPossibles(?string $valeurs_possibles): Parametre
    {
        $this->valeurs_possibles = $valeurs_possibles;
        return $this;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(int $ordre = Parametre::DEFAULT_ORDER): Parametre
    {
        $this->ordre = $ordre;
        return $this;
    }




}

