<?php

namespace UnicaenParametre\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class ParametrePrivileges extends Privileges
{
    const PARAMETRE_AFFICHER = 'parametre-parametre_afficher';
    const PARAMETRE_AJOUTER = 'parametre-parametre_ajouter';
    const PARAMETRE_MODIFIER = 'parametre-parametre_modifier';
    const PARAMETRE_SUPPRIMER = 'parametre-parametre_supprimer';
    const PARAMETRE_VALEUR = 'parametre-parametre_valeur';
}
