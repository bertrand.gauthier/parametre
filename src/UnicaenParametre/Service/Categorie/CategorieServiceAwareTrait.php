<?php

namespace UnicaenParametre\Service\Categorie;

trait CategorieServiceAwareTrait {

    private CategorieService $parametreCategorieService;

    public function getCategorieService(): CategorieService
    {
        return $this->parametreCategorieService;
    }

    public function setCategorieService(CategorieService $parametreCategorieService): void
    {
        $this->parametreCategorieService = $parametreCategorieService;
    }

}