<?php

namespace UnicaenParametre\Exception;

use Exception;

class ParametreNotFoundException extends Exception {}