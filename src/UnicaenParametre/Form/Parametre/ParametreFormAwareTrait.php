<?php

namespace UnicaenParametre\Form\Parametre;

trait ParametreFormAwareTrait {

    private ParametreForm $parametreForm;

    public function getParametreForm(): ParametreForm
    {
        return $this->parametreForm;
    }

    public function setParametreForm(ParametreForm $parametreForm): void
    {
        $this->parametreForm = $parametreForm;
    }
}